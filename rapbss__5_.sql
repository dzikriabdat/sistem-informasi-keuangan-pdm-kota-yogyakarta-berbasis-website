-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jun 2021 pada 01.30
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rapbss`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akp`
--

CREATE TABLE `akp` (
  `id` int(5) NOT NULL,
  `name_kelas` varchar(256) NOT NULL,
  `no_kelas` varchar(3) NOT NULL,
  `id_cf_s` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggaran_belanja`
--

CREATE TABLE `anggaran_belanja` (
  `id` int(5) NOT NULL,
  `name` varchar(256) NOT NULL,
  `id_identitas_sekolah` int(5) NOT NULL,
  `id_kategori_belanja` int(5) NOT NULL,
  `id_sub_belanja` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggaran_belanja`
--

INSERT INTO `anggaran_belanja` (`id`, `name`, `id_identitas_sekolah`, `id_kategori_belanja`, `id_sub_belanja`) VALUES
(1, 'Pengadaan ATK KBM', 1, 4, 1),
(2, 'MOS/Fortasi', 1, 4, 2),
(10, 'Penyusunan Krit.Kenaikan Kls/kllusan', 1, 2, 3),
(12, 'Penyusunan Pembagian Tugas Guru ', 1, 3, 4),
(13, 'Pengadaan Alat Pembelajaran ', 1, 4, 1),
(14, 'Pelaksanaan Hari Raya Idul Adha', 1, 4, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggaran_pendapatan`
--

CREATE TABLE `anggaran_pendapatan` (
  `id` int(5) NOT NULL,
  `name` varchar(256) NOT NULL,
  `no_uraian` int(3) NOT NULL,
  `id_identitas_sekolah` int(5) NOT NULL,
  `id_kategori_pendapatan` int(5) NOT NULL,
  `id_sub_pendapatan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggaran_pendapatan`
--

INSERT INTO `anggaran_pendapatan` (`id`, `name`, `no_uraian`, `id_identitas_sekolah`, `id_kategori_pendapatan`, `id_sub_pendapatan`) VALUES
(1, 'kelas 1', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_api`
--

CREATE TABLE `app_api` (
  `id` int(5) NOT NULL,
  `mailsender_address` varchar(150) DEFAULT NULL,
  `mailsender_password` varchar(150) DEFAULT NULL,
  `update_by` int(5) DEFAULT NULL,
  `update_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `app_api`
--

INSERT INTO `app_api` (`id`, `mailsender_address`, `mailsender_password`, `update_by`, `update_at`) VALUES
(1, 'hermindha@gmail.com', 'cc18ec0e16f4d1665248a2acf46fa1ff', 1, '2021-06-15 19:04:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_menu`
--

CREATE TABLE `app_menu` (
  `id` int(5) NOT NULL,
  `menu` varchar(128) NOT NULL,
  `url` varchar(50) NOT NULL,
  `order` int(2) NOT NULL,
  `icon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `app_menu`
--

INSERT INTO `app_menu` (`id`, `menu`, `url`, `order`, `icon`) VALUES
(1, 'Dashboard', 'dashboard', 1, 'fa fa-home'),
(2, 'Profile', 'profile_detail', 2, 'fa fa-user'),
(3, 'User', 'user', 3, 'fa fa-users'),
(4, 'Tools', 'tools', 4, 'fa fa-wrench'),
(5, 'Media', 'media', 5, 'fa fa-file-image-o'),
(6, 'Setting', 'setting', 6, 'fa fa-gear'),
(7, 'Logout', 'auth/logout', 20, 'fa fa-sign-out'),
(8, 'kategori', 'categories', 8, 'fa fa-book'),
(9, 'Pages', 'pages', 9, 'fa fa-file-text'),
(10, 'Home Page Setting', 'homepage', 10, 'fa fa-globe'),
(11, 'Bendahara Sekolah', 'bendaharasekolah', 11, 'fa fa-user'),
(12, 'Kepala Sekolah', 'kepalasekolah', 12, 'fa fa-user'),
(13, 'Bendahara PDM', 'bendaharapdm', 13, 'fa fa-user'),
(14, 'Identita Sekolah', 'identitasekolah', 14, 'fa fa-institution'),
(15, 'Rancangan Anggaran', 'anggaran', 15, 'fa fa-database'),
(16, 'Arus Kas Proyeksi', 'akp', 16, 'fa fa-file-text'),
(17, 'RPBS', 'rapbs', 17, 'fa fa-book'),
(18, 'Verifikasi Akun Sekolah', 'approve', 18, 'fa fa-users'),
(19, 'Daftar Sekolah', 'daftarsekolah', 19, 'fa fa-globe'),
(20, 'Kategori', 'master', 20, 'fa fa-book');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_setting`
--

CREATE TABLE `app_setting` (
  `id` int(5) NOT NULL,
  `site_title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `site_tagline` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_notification` int(1) DEFAULT NULL,
  `phone_notification` int(1) DEFAULT NULL,
  `mail_notification_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `phone_notification_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_social`
--

CREATE TABLE `app_social` (
  `id` int(5) NOT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `whatapps` varchar(100) DEFAULT NULL,
  `gmail` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `address` text,
  `last_updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `No_Telp` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `app_social`
--

INSERT INTO `app_social` (`id`, `instagram`, `youtube`, `facebook`, `whatapps`, `gmail`, `linkedin`, `twitter`, `address`, `last_updated`, `No_Telp`) VALUES
(1, 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', 'https://www.instagram.com/', '2021-06-14 12:18:35', '12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_sub_menu`
--

CREATE TABLE `app_sub_menu` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `name` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `order` int(2) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `app_sub_menu`
--

INSERT INTO `app_sub_menu` (`id`, `menu_id`, `name`, `url`, `icon`, `order`, `is_active`) VALUES
(1, 6, 'Menu Setting', 'menu', 'fa fa-list', 1, 1),
(2, 6, 'Role Access', 'role_access', '', 2, 1),
(3, 6, 'Role', 'role', '', 3, 1),
(4, 6, 'Api', 'api', '', 4, 1),
(5, 6, 'General', 'general', '', 5, 1),
(6, 6, 'Sosial Media dan Alamat', 'social', '', 6, 1),
(8, 8, 'Kategori Anggaran Belanja', 'kategorianggaranbelanja', '', 1, 1),
(9, 4, 'Backup DB', 'dump', '', 1, 1),
(10, 4, 'Mail Sender', 'mail', '', 2, 1),
(11, 11, 'Daftar Bendahara Sekolah', 'bendaharasekolah', '', 1, 1),
(12, 11, 'Persetujuan Akun Bendahara Sekolah', 'approve', '', 2, 1),
(13, 15, 'Anggaran Pendapatan', 'anggaranpendapatan', '', 1, 1),
(14, 15, 'Anggaran Belanja', 'belanja', '', 2, 1),
(15, 16, 'AKP Per-semester', 'akpper-semester', '', 1, 1),
(16, 16, 'AKP Per-bulan', 'akp', '', 2, 1),
(17, 19, 'SD', 'sd', '', 1, 1),
(18, 19, 'SMP', 'smp', '', 2, 1),
(21, 17, 'RAPBS', 'rapbs', '', 1, 1),
(22, 17, 'Daftar ACC RAPBS', 'daftaraccrapbs', '', 2, 1),
(23, 17, 'ACC RAPBS', 'accrapbs', '', 3, 1),
(25, 20, 'Kategori Anggaran Belanja', 'categoriesbelanja', '', 1, 1),
(26, 20, 'Kategori Anggaran Pendapatan', 'categoriesbelanja', '', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cf_s`
--

CREATE TABLE `cf_s` (
  `id` int(5) NOT NULL,
  `name_kelas` varchar(256) NOT NULL,
  `no_kelas` varchar(3) NOT NULL,
  `pp_bulan` int(12) NOT NULL,
  `id_anggaran_belanja` int(5) NOT NULL,
  `id_anggaran_pendapatan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `create`
--

CREATE TABLE `create` (
  `id` int(5) NOT NULL,
  `id_sekunder` int(5) NOT NULL,
  `created_by` int(5) DEFAULT NULL,
  `creat_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas_sekolah`
--

CREATE TABLE `identitas_sekolah` (
  `id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `kelurahan` varchar(15) NOT NULL,
  `kecamatan` varchar(15) NOT NULL,
  `kabupaten` varchar(15) NOT NULL,
  `kode_post` varchar(5) NOT NULL,
  `no.telp` varchar(12) NOT NULL,
  `user_bendahara` int(5) NOT NULL,
  `user_kepsek` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `identitas_sekolah`
--

INSERT INTO `identitas_sekolah` (`id`, `name`, `alamat`, `kelurahan`, `kecamatan`, `kabupaten`, `kode_post`, `no.telp`, `user_bendahara`, `user_kepsek`, `created_at`, `status`) VALUES
(1, 'smp', 'jl. uwuw', 'kel', 'oyy', 'yogyakarta', '12345', '0999887', 1, 1, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_anggaran_belanja`
--

CREATE TABLE `kategori_anggaran_belanja` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_anggaran_belanja`
--

INSERT INTO `kategori_anggaran_belanja` (`id`, `name`, `deleted`) VALUES
(2, 'Standart Kompetensi Kelulusan', 0),
(3, 'Standart Isi', 0),
(4, 'Standart Proses', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_anggaran_pendapatan`
--

CREATE TABLE `kategori_anggaran_pendapatan` (
  `id` int(5) NOT NULL,
  `name` varchar(256) NOT NULL,
  `daleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_anggaran_pendapatan`
--

INSERT INTO `kategori_anggaran_pendapatan` (`id`, `name`, `daleted`) VALUES
(1, 'pendapatan asli sekolah', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_sekolah`
--

CREATE TABLE `master_sekolah` (
  `id` int(5) NOT NULL,
  `name` int(128) NOT NULL,
  `daleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapbs`
--

CREATE TABLE `rapbs` (
  `id` int(5) NOT NULL,
  `name_kelas` varchar(256) NOT NULL,
  `no_kelas` varchar(3) NOT NULL,
  `id_anggaran_pendapatan` int(5) NOT NULL,
  `id_anggaran_belanja` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rapbs`
--

INSERT INTO `rapbs` (`id`, `name_kelas`, `no_kelas`, `id_anggaran_pendapatan`, `id_anggaran_belanja`) VALUES
(1, 'aaa', '1', 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekunder`
--

CREATE TABLE `sekunder` (
  `id` int(5) NOT NULL,
  `id_index` int(5) DEFAULT NULL,
  `id_indexp` int(5) DEFAULT NULL,
  `unit` int(12) NOT NULL,
  `nilai` int(12) NOT NULL,
  `frek` int(12) NOT NULL,
  `jumlah` int(12) NOT NULL,
  `user_bendahara` int(5) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sekunder`
--

INSERT INTO `sekunder` (`id`, `id_index`, `id_indexp`, `unit`, `nilai`, `frek`, `jumlah`, `user_bendahara`, `tanggal`) VALUES
(1, 2, NULL, 3, 5, 2, 8, 1, '0000-00-00 00:00:00'),
(2, 1, NULL, 2, 5, 2, 0, 1, '0000-00-00 00:00:00'),
(7, 12, NULL, 3, 5, 5, 5, 1, '0000-00-00 00:00:00'),
(10, 10, NULL, 2, 5, 5, 1, 1, '0000-00-00 00:00:00'),
(11, 14, NULL, 2, 5, 2, 8, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_kategori_belanja`
--

CREATE TABLE `sub_kategori_belanja` (
  `id` int(5) NOT NULL,
  `name` varchar(128) NOT NULL,
  `daleted` int(1) NOT NULL DEFAULT '0',
  `id_kategori_belanja` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_kategori_belanja`
--

INSERT INTO `sub_kategori_belanja` (`id`, `name`, `daleted`, `id_kategori_belanja`) VALUES
(1, 'Kegiatan Pengelolaan Kegiatan Belajar Mengajar', 0, 4),
(2, 'Program Kesiswaan', 0, 4),
(3, 'Standart Kompetensi Kelulusan', 0, 2),
(4, 'Standart isi', 0, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_kategori_pendapatan`
--

CREATE TABLE `sub_kategori_pendapatan` (
  `id` int(5) NOT NULL,
  `name` varchar(128) NOT NULL,
  `daleted` int(1) NOT NULL DEFAULT '0',
  `id_kategori_pendapatan` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_kategori_pendapatan`
--

INSERT INTO `sub_kategori_pendapatan` (`id`, `name`, `daleted`, `id_kategori_pendapatan`) VALUES
(1, 'spp', 0, 1),
(2, 'amal jaria', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `update`
--

CREATE TABLE `update` (
  `id` int(5) NOT NULL,
  `id_sekunder` int(5) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `update_by` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `username` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `image` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(5) NOT NULL,
  `is_active` int(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cookie` varchar(200) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `image`, `password`, `role_id`, `is_active`, `deleted`, `date_created`, `cookie`, `update_at`) VALUES
(1, 'hermindha', 'rara', 'hermindha@gmail.com', '', '25d55ad283aa400af464c76d713c07ad', 1, 1, 0, '2021-06-13 17:09:23', NULL, NULL),
(2, 'regita', 'regita', 're@gmail', '', '45a803af1b03198772816b2ce03b26df', 1, 1, 0, '2021-06-16 02:15:15', NULL, NULL),
(3, 'kepala', 'kepala', 'kepala@gmail.com', '', '870f669e4bbbfa8a6fde65549826d1c4', 3, 1, 0, '2021-06-16 02:15:15', NULL, NULL),
(4, 'bpdm', 'bpdm', 'bpdm@gmail', '', '4e79c20be3868567c6c68b30311d5213', 4, 1, 0, '2021-06-16 02:17:19', NULL, NULL),
(5, 'haha', 'haha', 'bsekolah@gmaill', '', '4e4d6c332b6fe62a63afe56171fd3725', 2, 1, 0, '2021-06-16 02:19:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `sub_menu_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`, `sub_menu_id`) VALUES
(114, 1, 1, NULL),
(115, 1, 2, NULL),
(116, 1, 3, NULL),
(117, 1, 4, NULL),
(122, 1, 4, 9),
(123, 1, 4, 10),
(118, 1, 5, NULL),
(119, 1, 6, NULL),
(124, 1, 6, 1),
(125, 1, 6, 2),
(126, 1, 6, 3),
(127, 1, 6, 4),
(128, 1, 6, 6),
(121, 1, 7, NULL),
(120, 1, 20, NULL),
(129, 1, 20, 25),
(130, 1, 20, 26),
(15, 2, 1, NULL),
(16, 2, 2, NULL),
(23, 2, 7, NULL),
(17, 2, 14, NULL),
(19, 2, 15, 13),
(18, 2, 15, 14),
(20, 2, 16, 15),
(21, 2, 16, 16),
(24, 2, 17, 21),
(22, 2, 17, 23),
(25, 3, 1, NULL),
(26, 3, 2, NULL),
(34, 3, 7, NULL),
(27, 3, 14, NULL),
(29, 3, 15, 13),
(28, 3, 15, 14),
(30, 3, 16, 15),
(31, 3, 16, 16),
(33, 3, 17, 22),
(32, 3, 17, 23),
(131, 4, 1, NULL),
(132, 4, 2, NULL),
(136, 4, 7, NULL),
(133, 4, 15, NULL),
(137, 4, 15, 14),
(134, 4, 18, NULL),
(135, 4, 19, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_bendahara_pdm`
--

CREATE TABLE `user_bendahara_pdm` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `status_kepegawaian` varchar(15) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `verified` int(1) NOT NULL DEFAULT '1',
  `verified_by` int(5) DEFAULT NULL,
  `verified_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verified_comen` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_bendahara_sekolah`
--

CREATE TABLE `user_bendahara_sekolah` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `status_kepegawaian` varchar(15) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `id_sekolah` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_bendahara_sekolah`
--

INSERT INTO `user_bendahara_sekolah` (`id`, `user_id`, `nip`, `status_kepegawaian`, `phone`, `status`, `id_sekolah`) VALUES
(1, 5, '33222', 'pns', '00000', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_kepala_pdm`
--

CREATE TABLE `user_kepala_pdm` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `status_kepegawaian` varchar(15) NOT NULL,
  `verified` int(1) NOT NULL DEFAULT '1',
  `verified_by` int(5) NOT NULL,
  `verified_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verified_comen` text,
  `nip` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_kepsek`
--

CREATE TABLE `user_kepsek` (
  `id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `status_kepegawaian` varchar(15) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `verified` int(1) DEFAULT '1',
  `verified_by` int(5) DEFAULT NULL,
  `verified_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verified_comen` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `id_sekolah` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(5) NOT NULL,
  `role` varchar(128) NOT NULL,
  `daleted` int(1) NOT NULL DEFAULT '0',
  `creat_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `daleted`, `creat_at`) VALUES
(1, 'Developer', 0, '2021-06-12 15:38:27'),
(2, 'Bendahara Sekolah', 0, '2021-06-12 15:38:27'),
(3, 'Kepala Sekolah', 0, '2021-06-12 15:38:27'),
(4, 'Bendaha PDM', 0, '2021-06-12 15:38:27'),
(5, 'Kepala PDM', 0, '2021-06-13 12:00:08');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akp`
--
ALTER TABLE `akp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cf_s` (`id_cf_s`);

--
-- Indeks untuk tabel `anggaran_belanja`
--
ALTER TABLE `anggaran_belanja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_identitas_sekolah` (`id_identitas_sekolah`),
  ADD KEY `id_kategori_belanja` (`id_kategori_belanja`,`id_sub_belanja`),
  ADD KEY `id_sub_belanja` (`id_sub_belanja`);

--
-- Indeks untuk tabel `anggaran_pendapatan`
--
ALTER TABLE `anggaran_pendapatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_identitas_sekolah` (`id_identitas_sekolah`),
  ADD KEY `id_kategori_pendapatan` (`id_kategori_pendapatan`,`id_sub_pendapatan`),
  ADD KEY `id_sub_pendapatan` (`id_sub_pendapatan`);

--
-- Indeks untuk tabel `app_api`
--
ALTER TABLE `app_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `update_by` (`update_by`);

--
-- Indeks untuk tabel `app_menu`
--
ALTER TABLE `app_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `app_setting`
--
ALTER TABLE `app_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `mail_notification` (`mail_notification`,`phone_notification`);

--
-- Indeks untuk tabel `app_sub_menu`
--
ALTER TABLE `app_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`menu_id`);

--
-- Indeks untuk tabel `cf_s`
--
ALTER TABLE `cf_s`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anggaran_belanja` (`id_anggaran_belanja`,`id_anggaran_pendapatan`),
  ADD KEY `id_anggaran_pendapatan` (`id_anggaran_pendapatan`);

--
-- Indeks untuk tabel `create`
--
ALTER TABLE `create`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sekunder` (`id_sekunder`,`created_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indeks untuk tabel `identitas_sekolah`
--
ALTER TABLE `identitas_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_anggaran_belanja`
--
ALTER TABLE `kategori_anggaran_belanja`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_anggaran_pendapatan`
--
ALTER TABLE `kategori_anggaran_pendapatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_sekolah`
--
ALTER TABLE `master_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rapbs`
--
ALTER TABLE `rapbs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_anggaran_pendapatan` (`id_anggaran_pendapatan`,`id_anggaran_belanja`),
  ADD KEY `id_anggaran_belanja` (`id_anggaran_belanja`);

--
-- Indeks untuk tabel `sekunder`
--
ALTER TABLE `sekunder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_bendahara` (`user_bendahara`),
  ADD KEY `id_index` (`id_index`),
  ADD KEY `id_indexp` (`id_indexp`);

--
-- Indeks untuk tabel `sub_kategori_belanja`
--
ALTER TABLE `sub_kategori_belanja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori_belanja` (`id_kategori_belanja`);

--
-- Indeks untuk tabel `sub_kategori_pendapatan`
--
ALTER TABLE `sub_kategori_pendapatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori_pendapatan` (`id_kategori_pendapatan`);

--
-- Indeks untuk tabel `update`
--
ALTER TABLE `update`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sekunder` (`id_sekunder`,`update_by`),
  ADD KEY `update_by` (`update_by`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_role` (`role_id`,`menu_id`,`sub_menu_id`),
  ADD KEY `id_menu` (`menu_id`),
  ADD KEY `id_sub_menu` (`sub_menu_id`);

--
-- Indeks untuk tabel `user_bendahara_pdm`
--
ALTER TABLE `user_bendahara_pdm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`verified_by`),
  ADD KEY `verified_by` (`verified_by`);

--
-- Indeks untuk tabel `user_bendahara_sekolah`
--
ALTER TABLE `user_bendahara_sekolah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indeks untuk tabel `user_kepala_pdm`
--
ALTER TABLE `user_kepala_pdm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`verified_by`),
  ADD KEY `verified_by` (`verified_by`);

--
-- Indeks untuk tabel `user_kepsek`
--
ALTER TABLE `user_kepsek`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`verified_by`),
  ADD KEY `verified_by` (`verified_by`),
  ADD KEY `id_sekolah` (`id_sekolah`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `akp`
--
ALTER TABLE `akp`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `anggaran_belanja`
--
ALTER TABLE `anggaran_belanja`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `anggaran_pendapatan`
--
ALTER TABLE `anggaran_pendapatan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `app_api`
--
ALTER TABLE `app_api`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `app_menu`
--
ALTER TABLE `app_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `app_setting`
--
ALTER TABLE `app_setting`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `app_sub_menu`
--
ALTER TABLE `app_sub_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `cf_s`
--
ALTER TABLE `cf_s`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `create`
--
ALTER TABLE `create`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `identitas_sekolah`
--
ALTER TABLE `identitas_sekolah`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kategori_anggaran_belanja`
--
ALTER TABLE `kategori_anggaran_belanja`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kategori_anggaran_pendapatan`
--
ALTER TABLE `kategori_anggaran_pendapatan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `master_sekolah`
--
ALTER TABLE `master_sekolah`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rapbs`
--
ALTER TABLE `rapbs`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `sekunder`
--
ALTER TABLE `sekunder`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `sub_kategori_belanja`
--
ALTER TABLE `sub_kategori_belanja`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `sub_kategori_pendapatan`
--
ALTER TABLE `sub_kategori_pendapatan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `update`
--
ALTER TABLE `update`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT untuk tabel `user_bendahara_pdm`
--
ALTER TABLE `user_bendahara_pdm`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_bendahara_sekolah`
--
ALTER TABLE `user_bendahara_sekolah`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user_kepala_pdm`
--
ALTER TABLE `user_kepala_pdm`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_kepsek`
--
ALTER TABLE `user_kepsek`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akp`
--
ALTER TABLE `akp`
  ADD CONSTRAINT `akp_ibfk_1` FOREIGN KEY (`id_cf_s`) REFERENCES `cf_s` (`id`);

--
-- Ketidakleluasaan untuk tabel `anggaran_belanja`
--
ALTER TABLE `anggaran_belanja`
  ADD CONSTRAINT `anggaran_belanja_ibfk_2` FOREIGN KEY (`id_identitas_sekolah`) REFERENCES `identitas_sekolah` (`id`),
  ADD CONSTRAINT `anggaran_belanja_ibfk_3` FOREIGN KEY (`id_sub_belanja`) REFERENCES `sub_kategori_belanja` (`id`),
  ADD CONSTRAINT `anggaran_belanja_ibfk_4` FOREIGN KEY (`id_kategori_belanja`) REFERENCES `kategori_anggaran_belanja` (`id`);

--
-- Ketidakleluasaan untuk tabel `anggaran_pendapatan`
--
ALTER TABLE `anggaran_pendapatan`
  ADD CONSTRAINT `anggaran_pendapatan_ibfk_1` FOREIGN KEY (`id_identitas_sekolah`) REFERENCES `identitas_sekolah` (`id`),
  ADD CONSTRAINT `anggaran_pendapatan_ibfk_3` FOREIGN KEY (`id_sub_pendapatan`) REFERENCES `sub_kategori_pendapatan` (`id`),
  ADD CONSTRAINT `anggaran_pendapatan_ibfk_4` FOREIGN KEY (`id_kategori_pendapatan`) REFERENCES `kategori_anggaran_pendapatan` (`id`);

--
-- Ketidakleluasaan untuk tabel `app_api`
--
ALTER TABLE `app_api`
  ADD CONSTRAINT `app_api_ibfk_1` FOREIGN KEY (`update_by`) REFERENCES `user` (`id`);

--
-- Ketidakleluasaan untuk tabel `app_setting`
--
ALTER TABLE `app_setting`
  ADD CONSTRAINT `app_setting_ibfk_1` FOREIGN KEY (`update_by`) REFERENCES `user` (`id`);

--
-- Ketidakleluasaan untuk tabel `app_sub_menu`
--
ALTER TABLE `app_sub_menu`
  ADD CONSTRAINT `app_sub_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `app_menu` (`id`);

--
-- Ketidakleluasaan untuk tabel `cf_s`
--
ALTER TABLE `cf_s`
  ADD CONSTRAINT `cf_s_ibfk_1` FOREIGN KEY (`id_anggaran_pendapatan`) REFERENCES `anggaran_pendapatan` (`id`),
  ADD CONSTRAINT `cf_s_ibfk_2` FOREIGN KEY (`id_anggaran_belanja`) REFERENCES `anggaran_belanja` (`id`);

--
-- Ketidakleluasaan untuk tabel `create`
--
ALTER TABLE `create`
  ADD CONSTRAINT `create_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `create_ibfk_2` FOREIGN KEY (`id_sekunder`) REFERENCES `sekunder` (`id`);

--
-- Ketidakleluasaan untuk tabel `rapbs`
--
ALTER TABLE `rapbs`
  ADD CONSTRAINT `rapbs_ibfk_1` FOREIGN KEY (`id_anggaran_belanja`) REFERENCES `anggaran_belanja` (`id`),
  ADD CONSTRAINT `rapbs_ibfk_2` FOREIGN KEY (`id_anggaran_pendapatan`) REFERENCES `anggaran_pendapatan` (`id`);

--
-- Ketidakleluasaan untuk tabel `sekunder`
--
ALTER TABLE `sekunder`
  ADD CONSTRAINT `sekunder_ibfk_1` FOREIGN KEY (`user_bendahara`) REFERENCES `user_bendahara_sekolah` (`id`),
  ADD CONSTRAINT `sekunder_ibfk_2` FOREIGN KEY (`id_index`) REFERENCES `anggaran_belanja` (`id`),
  ADD CONSTRAINT `sekunder_ibfk_3` FOREIGN KEY (`id_indexp`) REFERENCES `anggaran_pendapatan` (`id`);

--
-- Ketidakleluasaan untuk tabel `sub_kategori_belanja`
--
ALTER TABLE `sub_kategori_belanja`
  ADD CONSTRAINT `sub_kategori_belanja_ibfk_1` FOREIGN KEY (`id_kategori_belanja`) REFERENCES `kategori_anggaran_belanja` (`id`);

--
-- Ketidakleluasaan untuk tabel `sub_kategori_pendapatan`
--
ALTER TABLE `sub_kategori_pendapatan`
  ADD CONSTRAINT `sub_kategori_pendapatan_ibfk_1` FOREIGN KEY (`id_kategori_pendapatan`) REFERENCES `kategori_anggaran_pendapatan` (`id`);

--
-- Ketidakleluasaan untuk tabel `update`
--
ALTER TABLE `update`
  ADD CONSTRAINT `update_ibfk_1` FOREIGN KEY (`update_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `update_ibfk_2` FOREIGN KEY (`id_sekunder`) REFERENCES `sekunder` (`id`);

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD CONSTRAINT `user_access_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `app_menu` (`id`),
  ADD CONSTRAINT `user_access_menu_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`),
  ADD CONSTRAINT `user_access_menu_ibfk_3` FOREIGN KEY (`sub_menu_id`) REFERENCES `app_sub_menu` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_bendahara_pdm`
--
ALTER TABLE `user_bendahara_pdm`
  ADD CONSTRAINT `user_bendahara_pdm_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_bendahara_pdm_ibfk_2` FOREIGN KEY (`verified_by`) REFERENCES `user` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_bendahara_sekolah`
--
ALTER TABLE `user_bendahara_sekolah`
  ADD CONSTRAINT `user_bendahara_sekolah_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_bendahara_sekolah_ibfk_2` FOREIGN KEY (`id_sekolah`) REFERENCES `identitas_sekolah` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_kepala_pdm`
--
ALTER TABLE `user_kepala_pdm`
  ADD CONSTRAINT `user_kepala_pdm_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_kepala_pdm_ibfk_2` FOREIGN KEY (`verified_by`) REFERENCES `user` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_kepsek`
--
ALTER TABLE `user_kepsek`
  ADD CONSTRAINT `user_kepsek_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_kepsek_ibfk_2` FOREIGN KEY (`verified_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_kepsek_ibfk_3` FOREIGN KEY (`id_sekolah`) REFERENCES `identitas_sekolah` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
